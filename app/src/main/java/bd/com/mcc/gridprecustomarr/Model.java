package bd.com.mcc.gridprecustomarr;

/**
 * Created by nasir3 on 8/30/18.
 */

public class Model {


    private String name;
    private String url;

    public Model(String name, String url)  {
        this.name= name;
        this.url= url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString()  {
        return name;
    }
}
