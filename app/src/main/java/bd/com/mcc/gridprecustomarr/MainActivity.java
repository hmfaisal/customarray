package bd.com.mcc.gridprecustomarr;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final GridView gridView = (GridView) findViewById(R.id.gridView);

        //

        Model o7planning = new Model("o7planning", "//o7planning.org");
        Model google = new Model("Google", "http://google.com");
        Model facebook = new Model("Facebook", "http://facebook.com");
        Model eclipse = new Model("Eclipse", "http://eclipse.org");
        Model yahoo = new Model("Yahoo", "http://yahoo.com");

        Model[] Models = new Model[]{o7planning, google, facebook, eclipse, yahoo};

        // android.R.layout.simple_list_item_1 is a constant predefined layout of Android.
        // used to create a GridView with simple GridItem (Only one TextView).

        ArrayAdapter<Model> arrayAdapter
                = new ArrayAdapter<Model>(this, android.R.layout.simple_list_item_1, Models);


        gridView.setAdapter(arrayAdapter);

        // When the user clicks on the GridItem
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Object o = gridView.getItemAtPosition(position);
                Model Model = (Model) o;
                Toast.makeText(MainActivity.this, "Selected :" + " " + Model.getName() + "\n(" + Model.getUrl() + ")",
                        Toast.LENGTH_LONG).show();
            }
        });
    }
}
